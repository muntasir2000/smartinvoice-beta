CREATE TABLE STOCK_INVOICE (
  Id         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Date       TIMESTAMP    DEFAULT current_timestamp,
  Count      INT NOT NULL,
  Total      INT NOT NULL,
  UserId     INT NOT NULL,
  SupplierId INT NOT NULL,
  FOREIGN KEY fk_user(UserId)
  REFERENCES USER (UserId),
  FOREIGN KEY fk_supplier(SupplierId)
  REFERENCES SUPPLIER (SupplierId)
);

CREATE TABLE STOCK_INVENTORY (
  Id             INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Serial         INT NOT NULL PRIMARY KEY,
  Cost           INT NOT NULL,
  Warranty       INT,
  Sold           BOOL                     DEFAULT FALSE,
  ManufacturerId TEXT,
  StockInvoiceId INT NOT NULL,
  ProductId      INT NOT NULL,
  FOREIGN KEY fk_stockInvoice(StockInvoiceId)
  REFERENCES STOCK_INVOICE (Id),
  FOREIGN KEY fk_product(ProductId)
  REFERENCES PRODUCT (Id)
);

CREATE TABLE SELL_INVOICE
(
  Id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Date            TIMESTAMP    DEFAULT current_timestamp,
  Total           INT NOT NULL,
  Count           INT NOT NULL,
  IsRetail        BOOL         DEFAULT TRUE,
  ShippingAdress  TEXT,
  ShipppingName   TEXT,
  ShippingContact TEXT,
  DelivaryCharge  TEXT,
  UserId          INT NOT NULL,
  CustomerId      INT NOT NULL,
  FOREIGN KEY fk_sellInvoiceUserId(UserId)
  REFERENCES USER (UserId),
  FOREIGN KEY fk_sellInvoiceCustomerId(CustomerId)
  REFERENCES CUSTOMER (Id)
);


CREATE TABLE SELL_RECORDS
(
  Id            INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  IsRetail      BOOL         DEFAULT TRUE,
  Serial        INT NOT NULL,
  SellInvoiceId INT NOT NULL,
  FOREIGN KEY fk_sellRecordsSellInvoiceId(SellInvoiceId)
  REFERENCES SELL_INVOICE (Id)
)


