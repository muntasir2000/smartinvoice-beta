CREATE TABLE USERS
(
UserId int NOT NULL PRIMARY KEY,
UserName varchar(50) NOT NULL,
UserAddress varchar(50),
UserContact nchar(11) NOT NULL
)

CREATE TABLE SUPPLIER
(
SupplierId int NOT NULL PRIMARY KEY,
SupplierName varchar(50) NOT NULL,
SupplierAddress varchar(50),
SupplierContact nchar(11) NOT NULL
)

CREATE TABLE STOCK_INVOICE
(
StockInvoiceId int NOT NULL IDENTITY(100,1) PRIMARY KEY,
StockInvoiceDate date DEFAULT GetDate(),
StockInvoiceCount int NOT NULL,
StockInvoiceTotal money NOT NULL,
UserId int NOT NULL 
FOREIGN KEY REFERENCES USERS(UserId),
SupplierId int NOT NULL 
FOREIGN KEY REFERENCES SUPPLIER(SupplierId)
)

CREATE TABLE CUSTOMER
(
CustomerId int NOT NULL IDENTITY(1000,1) PRIMARY KEY,
CustomerName varchar(50) NOT NULL,
CustomerAddress varchar(50),
CustomerContact nchar(11) NOT NULL
)

CREATE TABLE SELL_INVOICE
(
SellInvoiceId int NOT NULL IDENTITY(200,1) PRIMARY KEY,
SellInvoiceDate date DEFAULT GetDate(),
SellInvoiceTotal money NOT NULL,
SellInvoiceCount int NOT NULL,
SellInvoiceDiscount int NOT NULL,
Is_retail text DEFAULT 'TRUE',
ShippingAdress text ,
Ship_To text,
ShippingContact text,
DelivaryCharge int,
UserId int NOT NULL 
FOREIGN KEY REFERENCES USERS(UserId),
CustomerId int NOT NULL 
FOREIGN KEY REFERENCES CUSTOMER(CustomerId)
)

CREATE TABLE PRODUCTS
(
ProductId int NOT NULL PRIMARY KEY,
ProductName varchar(50) NOT NULL,
ProductDescription text NOT NULL,
Wholesale int DEFAULT 0,
Retail int DEFAULT 0,
ProductCode int 
)
CREATE TABLE STOCK_INVENTORY
(
ItemId int NOT NULL PRIMARY KEY,
StockInventoryDate date DEFAULT GetDate(),/*DELETED*/
ItemSerial int NOT NULL,
Cost int NOT NULL,
Warranty int, 
Sold text DEFAULT 'FALSE',/*BOOL*/
ManufacturerId int, /*TEXT*/
StockInvoiceId int NOT NULL 
FOREIGN KEY REFERENCES STOCK_INVOICE(StockInvoiceId),
ProductId int NOT NULL
FOREIGN KEY REFERENCES PRODUCTS(ProductId)
)

CREATE TABLE SELL_RECORDS
(
SellRecordsId int NOT NULL PRIMARY KEY,
Cost int NOT NULL,
Warranty int DEFAULT 0, 
Is_Retail text DEFAULT 'TRUE',
SellInvoiceId int NOT NULL 
FOREIGN KEY REFERENCES SELL_INVOICE(SellInvoiceId),
ItemId int NOT NULL 
FOREIGN KEY REFERENCES STOCK_INVENTORY(ItemId),

)
