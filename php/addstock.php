<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/16/15
 * Time: 11:22 AM
 */
?>


<!DOCTYPE html>

<?php
session_start();
if (isset($_SESSION['login_email'])) { //user is logged in


} else {
    session_destroy();
     header("Location: ../index.html");

}
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>


    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>
<div id="wrapper">


    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                        Williams</strong>
                             </span> <span class="text-muted text-xs block">Sales Team <b
                                        class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="login.html">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li>
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>
                            <li>
                                <a href="wholesale.php">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li class="active">
                        <a href="#"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="managecustomer.php">Add/Edit Customer</a>
                            </li>
                            <li>
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageproduct.php">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.html">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="serialtag.php">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png"
                             width="200" height="37">
                    </a>
                </div>
            </div>
        </nav>

        <h1>Stock Entry</h1>

        <div class="row">
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Supplier Info
                    </div>
                    <div class="ibox-content">
                        <div class="form-group" id="supplier-typeahead">
                            <label class="sr-only">Name</label>
                            <!--<input class="typeahead form-control" autocomplete="off" spellcheck="false"
                                   placeholder="Supplier Name">-->
                            <select class="form-control" id="supplierName">
                                <?php
                                require_once('functions.php');

                                $connection = connect_db();
                                $statement = $connection->prepare('SELECT Name FROM SUPPLIER');
                                $statement->execute();

                                while($row = $statement->fetch(PDO::FETCH_ASSOC)){
                                    echo '<option>'.$row['Name'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <button class="btn btn-primary">Add New Supplier</button>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Invoice Date
                    </div>
                    <div class="ibox-content">
                        <input type="date" id="dateInput">
                        <script>
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!

                            var yyyy = today.getFullYear();
                            if(dd<10){
                                dd='0'+dd
                            }
                            if(mm<10){
                                mm='0'+mm
                            }
                            var today = yyyy + '-' + mm + '-' + dd;
                            document.getElementById("dateInput").value = today;
                        </script>
                    </div>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Items
                    </div>
                    <div class="ibox-content">
                        <table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Serial no.</th>
                                <th>Manu. ID</th>
                                <th>Warranty</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--add new item -->
            <div class="col-md-6">
                <h4>Add new item</h4>

                <div class="form-group">
                    <!--<label class="sr-only" for="exampleInputEmail3">Item Name</label>
                    <input type="text" class="form-control" id="nameInput" placeholder="Name">-->
                    <select id="productName" class="form-control">
                        <?php
                            require_once('functions.php');

                            $connection = connect_db();
                            $statement = $connection->prepare('SELECT Name, Description FROM PRODUCT');
                            $statement->execute();

                            while($row = $statement->fetch(PDO::FETCH_ASSOC)){
                                echo '<option>'.$row['Name'].'</option>';
                            }
                        ?>

                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword3">Description</label>
                    <input type="text" class="form-control" id="descriptionInput" placeholder="Description" required>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword3">Serial No.</label>
                    <input type="number" class="form-control" id="serialInput" placeholder="Serial No." required>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword3">Manufacturer ID</label>
                    <input type="text" class="form-control" id="manIdInput" placeholder="Manufacturer ID">
                </div>
                <div class="form-group input-group">
                    <label class="sr-only" for="inputWarranty">Warranty(Days)</label>
                    <input type="text" class="form-control" id="inputWarranty" placeholder="Warranty in Days" aria-describedby="basic-addon2">
                    <span class="input-group-addon" id="basic-addon2">Optional</span>

                </div>



                <div class="form-group">
                    <label class="sr-only" for="exampleInputPassword3">Buying price</label>
                    <input type="text" class="form-control" id="priceInput" placeholder="Buying price">
                </div>

                <button class="btn btn-primary" id="addNewItemBtn">Add New Item</button>
            </div>

            <div class="col-md-4 pull-right">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-2">Total</label>

                        <div class="col-xs-10">
                            <input id="totalPrice" class="form-control" type="text" placeholder="Total" value="0">
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button id="saveBtn" class="btn btn-primary col-md-2 pull-right" style="margin-bottom: 20px">Save</button>
            </div>
        </div>


    </div>
</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../bower_components/typeahead.js/dist/typeahead.bundle.min.js"

<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../assets/js/addstock.js"></script>

</body>
</html>

