<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/16/15
 * Time: 1:33 AM
 */

?>

<!DOCTYPE html>

<?php
session_start();
if (isset($_SESSION['login_email'])) { //user is logged in


} else { //user is not logged, shouldn't see this page
    // header("Location: index.html");

}
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>
    <!--
        <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
        <link rel="stylesheet" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../assets/css/main.css">
        <link rel="stylesheet" href="../assets/css/sb-admin-2.css">

        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>-->

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>
<div id="wrapper">


    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <!--<div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="index.html">SmartInvoice</a>
        </div>

        <ul class="nav navbar-top-links navbar-right">

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <?php
        /*                    echo $_SESSION['loggedOnUserFullName'];
                            */ ?>
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>

                    <li class="divider"></li>
                    <li><a href="endpoints/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>-->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                        Williams</strong>
                             </span> <span class="text-muted text-xs block">Sales Team <b
                                        class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="login.html">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li>
                        <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>
                            <li>
                                <a href="wholesale.html">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li class="active">
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href=managecustomer.html">Add/Edit Customer</a>
                            </li>
                            <li class="active">
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageproduct.php">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.html">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>

                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!--end navbar and sidebar contents-->

    <div id="page-wrapper" class="gray-bg">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png"
                             width="200" height="37">
                    </a>
                </div>
            </div>
        </nav>

        <h1>Manage Suppliers</h1>

        <!--available suppliers -->
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Available Suppliers
                    </div>
                    <div class="ibox-content">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" style="padding-bottom: 0px">

                            <table id="data-table" class="table table-striped table-bordered " cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Contact</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <button id="removeSupplier" class="btn btn-danger">Remove Selected Suppliers</button>
                    </div>
                </div>
            </div>

            <!--end available suppliers-->

            <!--add new supplier-->
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Add new Supplier
                    </div>
                    <div class="ibox-content">
                        <div class="">
                            <div class="form-group">
                                <label>Name</label>
                                <input id='nameInput' class="form-control" type="text" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea id='addressInput' class="form-control" type="text"
                                          placeholder="Address"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <input id='contactInput' class="form-control" type="text"
                                       placeholder="Contact Information">
                            </div>

                            <div class="form-group">
                                <button id="saveNewSupplierButton" class="btn btn-danger col-md-3">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--end add new supplier-->
        </div>
    </div>
</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../assets/js/managesupplier.js"></script>

</body>
</html>