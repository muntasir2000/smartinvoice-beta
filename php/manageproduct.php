<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/14/15
 * Time: 12:36 AM
 */
session_start();
if (!isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    session_destroy();
    header('location: ../index.html');
}
?>

<html lang="en">


<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>


    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">


</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <!--<div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

            </button>
            <a class="navbar-brand" href="index.html">SmartInvoice</a>
        </div>

        <ul class="nav navbar-top-links navbar-right">

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <?php
/*                    echo $_SESSION['loggedOnUserFullName'];
                    */?>
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>

                    <li class="divider"></li>
                    <li><a href="endpoints/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
            </li>
        </ul>-->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                        Williams</strong>
                             </span> <span class="text-muted text-xs block">Sales Team <b
                                        class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="login.html">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <li>
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>
                            <li>
                                <a href="wholesale.html">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li class="active">
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href=managecustomer.html">Add/Edit Customer</a>
                            </li>
                            <li>
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li class="active">
                                <a href="#">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.html">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>

                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!--end navbar and sidebar contents-->

    <div id="page-wrapper" class="gray-bg">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png" width="200" height="37">
                    </a>
                </div>
            </div>
        </nav>
        <h1>Manage Products</h1>
        <div class="row">
            <div class="col-md-12">
<!--                <div class="panel panel-info panel-sharp-border ">-->
                <div class="ibox float-e-margins ">
                    <div class="ibox-title">
                        <h4>Available Products</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" style="padding-bottom: 0px">
                            <table id="data-table" class="table table-striped table-bordered dataTable "
                                   cellspacing="0" width="100%" >
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Wholesale Price</th>
                                    <th>Retail Price</th>
                                    <th>Product Code</th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                        <button id="removeProduct" class="btn btn-danger">Remove Selected Product</button>
                    </div>
                </div>
            </div>

        <div class="col-md-12">
                <div class="ibox float-e-margins ">
                    <div class="ibox-title">
                        <h4>Add new Product</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="span6">
                            <div class="form-group">
                                <label>Name</label>
                                <input id='nameInput' class="form-control" type="text" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id='descInput' class="form-control" type="text"
                                          placeholder="Description"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Wholesale Price</label>
                                <input id='wholesaleInput' class="form-control" type="number"
                                       placeholder="Wholesale Price">
                            </div>
                            <div class="form-group">
                                <label>Retail Price</label>
                                <input id='retailInput' class="form-control" type="number" placeholder="Retail Price">
                            </div>
                            <div class="form-group">
                                <label>Product Code</label>
                                <input id='productCodeInput' class="form-control" type="number"
                                       placeholder="Product Code">
                            </div>
                            <div class="form-group">
                                <button id="saveNewProductButton" class="btn btn-primary col-md-2">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </div>
    </div>


</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../assets/js/manageproduct.js"></script>

</body>
</html>
