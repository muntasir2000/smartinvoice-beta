<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/22/15
 * Time: 4:00 PM
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_GET['new'])) {
        // requested

        $incoming = $_GET['new'];

        //incoming data
        $customer = $incoming['customer'];
        $items = $incoming['items'];
        $total = $incoming['total'];
        $delivery = $incoming['delivery'];
        $shippingName = $incoming['shippingname'];
        $shippingAddress = $incoming['shippingaddress'];
        $count = count($items);

        $isRetail = false;
        if (strcmp($incoming['type'], 'retail')) {
            $isRetail = true;
        }

        /*TODO Decide Date field should be added to retail sale or not */
        $date = date("Y-m-d H:i:s");

        try {
            $connection = connect_db();
            $statement = $connection->prepare('SELECT Id FROM CUSTOMER WHERE Name = :customer');
            $statement->execute(array('customer' => $customer));

            $customerId = $statement->fetch(PDO::FETCH_ASSOC)['Id'];

            $statement = $connection->prepare('
                INSERT INTO
                  SELL_INVOICE (
                    Date,
                    Total,
                    Count,
                    IsRetail,
                    ShipppingName,
                    ShippingAdress,
                    DelivaryCharge,
                    UserId,
                    CustomerId)
                VALUES(
                    :date,
                    :total,
                    :count,
                    :isretail,
                    :shippingname,
                    :shippingaddress,
                    :delivery,
                    :userid,
                    :customerid
                )');

            $statement->execute(
                array(
                    'date' => $date,
                    'total' => $total,
                    'count' => $count,
                    'isretail' => $isRetail,
                    'shippingname' => $shippingName,
                    'shippingaddress' => $shippingAddress,
                    'delivery' => $delivery,
                    'userid' => $_SESSION['loggedOnUserId'],
                    'customerid' => $customerId
                )
            );

            $sellInvoiceId = $connection->lastInsertId();

            /*insert into sell records table and
             update STOCK_INVENTORY Sold attribute to mark the item as sold*/

            foreach ($items as $singleItem) {

                //first insert item into sell records
                $statement = $connection->prepare('
                  INSERT INTO
                    SELL_RECORDS (
                        SellInvoiceId, IsRetail, Serial
                    )
                    VALUES (
                        :sellInvoiceId,
                        :isretail,
                        :serial
                    )');

                $statement->execute(array(
                    'sellInvoiceId' => $sellInvoiceId,
                    'isretail' => $isRetail,
                    'serial' => $singleItem
                ));

                //then mark the inserted item as sold
                $statement = $connection->prepare('
                UPDATE STOCK_INVENTORY
                    SET Sold = TRUE
                    WHERE Serial = :serial;');

                $statement->execute(array('serial' => $singleItem));

            }

            header('Content-type: application/json');
            echo json_encode($sellInvoiceId);

        } catch (PDOException $e) {
            echo $e;
            echo http_response_code(400);
        }
    }
} else {
    echo http_response_code(401);
}
