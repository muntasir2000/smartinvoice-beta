<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/23/15
 * Time: 3:09 PM
 */


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_POST['userId'])) {
        //todo make product-code column input uppercase
        //deletion of user requested

        $userId = $_POST['userId'];
        try {
            $connection = connect_db();
            $statement = $connection->prepare('DELETE FROM `USER` WHERE UserId = ' . $userId);
            $statement->execute();
            echo http_response_code(200);

        } catch (PDOException $e) {
            echo http_response_code(400);

        }

    } else if (isset($_GET['loadUser'])) {
        $response = array();

        try {
            $connection = connect_db();

            $statement = $connection->prepare('SELECT * FROM `USER`');
            $statement->execute();

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($response, array(
                    'id' => $row['UserId'],
                    'email' => $row['Email'],
                    'fullname' => $row['FullName'],
                    'designation' => $row['Designation']

                ));
            }

        } catch (PDOException $pe) {
            echo $pe->getMessage();
        }

        header('Content-type: application/json');
        echo json_encode($response);

    } else if (isset($_GET['newuser'])) {
        $newUser = $_GET['newuser'];
        // echo print_r($newProduct);
        $password = $newUser['pass'];
        $password = hash('SHA512', $password);

        try {
            $connection = connect_db();
            $statement = $connection->prepare('
              INSERT INTO USER
              (FullName, Email, PassHash, Designation)
              VALUES (
                :name,
                :email,
                :pass,
                :designation)');

            $statement->execute(array(
                'name' => $newUser['fullname'],
                'email' => $newUser['email'],
                'pass' => $password,
                'designation' =>$newUser['designation']
            ));

            echo http_response_code(200);
        } catch (PDOException $ex) {
//            echo http_response_code(401);
            echo $ex;
        }

    }
} else {
    echo http_response_code(401);
}
