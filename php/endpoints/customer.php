<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/23/15
 * Time: 1:28 PM
 */


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_POST['customerId'])) {
        //todo make product-code column input uppercase
        //deletion of customer requested

        $customerId = $_POST['customerId'];
        try {
            $connection = connect_db();
            $statement = $connection->prepare('DELETE FROM `CUSTOMER` WHERE Id = ' . $customerId);
            $statement->execute();
            echo http_response_code(200);

        } catch (PDOException $e) {
            echo http_response_code(400);

        }

    } else if (isset($_GET['loadCustomer'])) {
        $response = array();

        try {
            $connection = connect_db();

            $statement = $connection->prepare('SELECT * FROM `CUSTOMER`');
            $statement->execute();

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($response, array(
                    'id' => $row['Id'],
                    'name' => $row['Name'],
                    'address' => $row['Address'],
                    'contact' => $row['Contact']

                ));
            }

        } catch (PDOException $pe) {
            echo $pe->getMessage();
        }

        header('Content-type: application/json');
        echo json_encode($response);

    } else if (isset($_GET['newcustomer'])) {
        $newCustomer = $_GET['newcustomer'];
        // echo print_r($newProduct);
        try {
            $connection = connect_db();
            $statement = $connection->prepare('
              INSERT INTO CUSTOMER
              (Name, Address, Contact)
              VALUES (
                :name,
                :address,
                :contact )');

            $statement->execute(array(
                'name' => $newCustomer['name'],
                'address' => $newCustomer['address'],
                'contact' => $newCustomer['contact']
            ));

            echo http_response_code(200);
        } catch (PDOException $ex) {
//            echo http_response_code(401);
            echo $ex;
        }

    }
} else {
    echo http_response_code(401);
}
