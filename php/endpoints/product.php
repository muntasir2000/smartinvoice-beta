<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/14/15
 * Time: 8:32 PM
 */

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_POST['product_code'])) {
        //todo make product-code column input uppercase
        //deletion of product requested

        $productCode = $_POST['product_code'];
        try {
            $connection = connect_db();
            $statement = $connection->prepare('DELETE FROM `PRODUCT` WHERE ProductCode = ' . $productCode);
            $statement->execute();
            echo http_response_code(200);

        } catch (PDOException $e) {
            echo http_response_code(400);

        }

    } else if (isset($_GET['loadproducts'])) {
        $response = array();

        try {
            $connection = connect_db();

            $statement = $connection->prepare('SELECT * FROM `PRODUCT`');
            $statement->execute();

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($response, array('name' => $row['Name'],
                        'desc' => $row['Description'],
                        'wholesale' => $row['WholesalePrice'],
                        'retail' => $row['RetailPrice'],
                        'productcode' => $row['ProductCode'])
                );
            }

        } catch (PDOException $pe) {
            echo $pe->getMessage();
        }

        header('Content-type: application/json');
        echo json_encode($response);

    } else if (isset($_GET['newproduct'])) {
        $newProduct = $_GET['newproduct'];
       // echo print_r($newProduct);
        try {
            $connection = connect_db();
            $statement = $connection->prepare('
              INSERT INTO PRODUCT
              (Name, Description, WholesalePrice, RetailPrice, ProductCode)
              VALUES (
                :name,
                :description,
                :wholesale,
                :retail,
                :productcode )');

            $statement->execute(array(
                'name'=>$newProduct['name'],
                'description'=>$newProduct['description'],
                'wholesale'=>$newProduct['wholesale'],
                'retail'=>$newProduct['retail'],
                'productcode'=>$newProduct['productcode']
            ));

            echo http_response_code(200);
        }catch (PDOException $ex){
//            echo http_response_code(401);
            echo $ex;
        }
    }
} else {
    echo http_response_code(401);
}
