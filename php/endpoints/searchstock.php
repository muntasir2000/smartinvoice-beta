<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/4/15
 * Time: 5:17 PM
 */


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_GET['serial'])) {
        // requested

        $incomingSerial = $_GET['serial'];



        try {
            $connection = connect_db();
            $statement = $connection->prepare('
              SELECT
                P.Name as Name,
                P.Description as Description,
                S.Serial as Serial,
                S.ManufacturerId as ManId,
                S.Warranty as Warranty,
                P.RetailPrice as Price
              FROM PRODUCT P, STOCK_INVENTORY S WHERE
                S.ProductId = P.Id AND
                S.Serial = :serial AND
                S.Sold = FALSE ');
            $statement->execute(array('serial'=> $incomingSerial));

            //should return a single stock item
            if($row = $statement->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['name'] = $row['Name'];
                $item['description'] = $row['Description'];
                $item['serial'] = $row['Serial'];
                $item['manid'] = $row['ManId'];
                $item['warranty'] = $row['Warranty'];
                $item['price'] = $row['Price'];

                $response = array();
                $response[] = $item;
                header('Content-type: application/json');
                echo json_encode($item);
            }else{
                echo 'no item found associated with this serial';
                http_response_code(400);
            }


        } catch (PDOException $e) {
            echo $e;
            echo http_response_code(400);

        }

    }
} else {
    echo http_response_code(401);
}
