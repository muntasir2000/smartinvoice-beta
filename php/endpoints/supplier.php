<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/16/15
 * Time: 2:20 AM
 */


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_POST['supplierId'])) {
        //todo make product-code column input uppercase
        //deletion of product requested

        $supplierId = $_POST['supplierId'];
        try {
            $connection = connect_db();
            $statement = $connection->prepare('DELETE FROM `SUPPLIER` WHERE Id = ' . $supplierId);
            $statement->execute();
            echo http_response_code(200);

        } catch (PDOException $e) {
            echo http_response_code(400);

        }

    } else if (isset($_GET['loadSupplier'])) {
        $response = array();

        try {
            $connection = connect_db();

            $statement = $connection->prepare('SELECT * FROM `SUPPLIER`');
            $statement->execute();

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($response, array(
                    'id' => $row['Id'],
                    'name' => $row['Name'],
                    'address' => $row['Address'],
                    'contact' => $row['Contact']

                ));
            }

        } catch (PDOException $pe) {
            echo $pe->getMessage();
        }

        header('Content-type: application/json');
        echo json_encode($response);

    } else if (isset($_GET['newsupplier'])) {
        $newSupplier = $_GET['newsupplier'];
        // echo print_r($newProduct);
        try {
            $connection = connect_db();
            $statement = $connection->prepare('
              INSERT INTO SUPPLIER
              (Name, Address, Contact)
              VALUES (
                :name,
                :address,
                :contact )');

            $statement->execute(array(
                'name' => $newSupplier['name'],
                'address' => $newSupplier['address'],
                'contact' => $newSupplier['contact']
            ));

            echo http_response_code(200);
        } catch (PDOException $ex) {
//            echo http_response_code(401);
            echo $ex;
        }

        /*Only used for bloodhound in stock entry*/
    } else if (isset($_REQUEST['searchsuppliername'])) {
        $supplierNameQuery = $_REQUEST['searchsuppliername'];
        $response = array();
        try {
            $connection = connect_db();
            $statement = $connection->prepare("
                SELECT Name FROM SUPPLIER WHERE Name LIKE '%" . $supplierNameQuery . "%'
            ");
            $statement->execute(array('supplierNameQuery' => $supplierNameQuery));

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($response, array(
                    'value' => $row['Name'],
                ));
            }
            header('Content-type: application/json');
            echo json_encode($response);

        } catch (PDOException $ex) {

        }
    }
} else {
    echo http_response_code(401);
}
