<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/14/15
 * Time: 8:32 PM
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

require_once('../functions.php');
session_start();

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {
    //user is logged in and authenticated(has valid session)
    if (isset($_GET['new'])) {
        // requested

        $incoming = $_GET['new'];

        //incoming data
        $supplier = $incoming['supplier'];
        $date = $incoming['date'];
        $items = $incoming['items'];
        $total = $incoming['total'];

        try {
            $connection = connect_db();
            $statement = $connection->prepare('SELECT Id FROM SUPPLIER WHERE Name = :supplier');
            $statement->execute(array('supplier' => $supplier));

            $supplierId = $statement->fetch(PDO::FETCH_ASSOC)['Id'];


            $productIdQueryString = 'SELECT Id, Name FROM PRODUCT WHERE ';
            $noOfItems = count($items) - 1;
            for ($i = 0; $i <= $noOfItems; $i++) {
                $singleItem = $items[$i];
                $productIdQueryString = $productIdQueryString . 'Name = \'' . $singleItem['name'] . '\'';

                if ($i != $noOfItems) {
                    $productIdQueryString = $productIdQueryString . ' OR ';
                }
            }

            echo $productIdQueryString;
            $statement = $connection->prepare($productIdQueryString);
            $statement->execute();

            $productNameIdMap = array();
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                print_r($row);
                $productNameIdMap[$row['Name']] = $row['Id'];
            }

            print_r($productNameIdMap);
            /*all necessary data is fetched. now insertion begins*/
            $statement = $connection->prepare('
                INSERT INTO STOCK_INVOICE
                  (Date, Count, Total, UserId, SupplierId)
                  VALUES (:date, :count, :total, :userid, :supplierid);
            ');

            $statement->execute(
                array(
                    'date' => $date,
                    'count' => $noOfItems + 1,
                    'total' => $total,
                    'userid' => $_SESSION['loggedOnUserId'],
                    'supplierid' => $supplierId
                )
            );

            $stockInvoiceId = $connection->lastInsertId();
            echo $stockInvoiceId;

            /*insert into stock inventory table.
            * each item
            */
            for($i=0; $i<=$noOfItems; $i++){
                $singleItem = $items[$i];
                echo 'name :' . $singleItem['name'];
                echo 'id :' . $productNameIdMap[$singleItem['name']];

                $statement = $connection->prepare('
                    INSERT INTO STOCK_INVENTORY
                    (Serial, Cost, Warranty, ManufacturerId, StockInvoiceId, ProductId)
                    VALUES (:serial, :cost, :warranty, :manid, :sid, :productid);
                ');
                $statement->execute(
                    array(
                        'serial'=>$singleItem['serial'],
                        'cost'=>$singleItem['cost'],
                        'warranty'=>$singleItem['warranty'],
                        'manid'=>$singleItem['manuId'],
                        'sid'=>$stockInvoiceId,
                        'productid'=>$productNameIdMap[$singleItem['name']]
                    )
                );
            }

            echo http_response_code(200);

        } catch (PDOException $e) {
            echo $e;
            echo http_response_code(400);

        }

    }
} else {
    echo http_response_code(401);
}
