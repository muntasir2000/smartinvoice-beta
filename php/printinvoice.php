<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/22/15
 * Time: 11:49 PM
 */

?>
<?php

require_once('functions.php');
$invoiceId = null;
$date = null;
$total = null;
$isRetail = null;
$shippingName = 'SELF';
$shippingAddress = 'SELF';
$deliveryCharge = null;
$customerName = null;
$customerAddress = null;
$items = array();

if (isset($_GET['id'])) {
    $invoiceId = $_GET['id'];

    try {
        $connection = connect_db();
        $statement = $connection->prepare('
            SELECT
              si.Date,
              si.Total,
              si.IsRetail,
              si.ShipppingName,
              si.ShippingAdress,
              si.DelivaryCharge,
              c.Name,
              c.Address
            FROM
              SELL_INVOICE si,
              CUSTOMER c
            WHERE
              si.CustomerId = c.Id AND
              si.Id = :invoiceid
            ');

        $statement->execute(array('invoiceid' => $invoiceId));

        if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $date = $row['Date'];
            $total = $row['Total'];
            $isRetail = $row['IsRetail'];
            if (isset($row['ShippingName']))
                $shippingName = $row['ShippingName'];
            if (isset($row['ShippingAddress']))
                $shippingAddress = $row['ShippingAddress'];
            if (isset($row['DeliveryCharge']))
                $deliveryCharge = $row['DeliveryCharge'];
            $customerName = $row['Name'];
            $customerAddress = $row['Address'];
        }

        //whether it is a retail sale or wholesale sale
        if ($isRetail === 0) {
            //wholesale
            $statement = $connection->prepare('
            SELECT
              P.Name,
              P.Description,
              SI.Serial,
              SI.ManufacturerId,
              SI.Warranty,
              P.WholesalePrice AS Price
            FROM
              SELL_RECORDS SR,
              STOCK_INVENTORY SI,
              PRODUCT P
            WHERE
              SR.Serial = SI.Serial AND
              SI.ProductId = P.Id AND
              SR.Id = :invoiceId
            ');

            $statement->execute(array('invoiceId' => $invoiceId));

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $items[] = array(
                    'name' => $row['Name'],
                    'description' => $row['Description'],
                    'serial' => $row['Serial'],
                    'manuid' => $row['ManufacturerId'],
                    'warranty' => $row['Warranty'],
                    'price' => $row['Price']
                );
            }

        } else {
            //retail sale
            $statement = $connection->prepare('
            SELECT
              P.Name,
              P.Description,
              SI.Serial,
              SI.ManufacturerId,
              SI.Warranty,
              P.RetailPrice AS Price
            FROM
              SELL_RECORDS SR,
              STOCK_INVENTORY SI,
              PRODUCT P
            WHERE
              SR.Serial = SI.Serial AND
              SI.ProductId = P.Id AND
              SR.Id = :invoiceId
              ');

            $statement->execute(array('invoiceId' => $invoiceId));

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $items[] = array(
                    'name' => $row['Name'],
                    'description' => $row['Description'],
                    'serial' => $row['Serial'],
                    'manuid' => $row['ManufacturerId'],
                    'warranty' => $row['Warranty'],
                    'price' => $row['Price']
                );
            }
        }


    } catch (PDOException $e) {
        echo $e;
    }

} else {
    echo 'invoice id not given';
}
?>


<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">


</head>
<body>
<div class="container" style="width: 900px; padding: .5in">
    <div class="row">
        <div class="col-md-6">
            <h1>SmartInvoice Inc.</h1>
            <h5>2/14, Road 8, Niketon</h5>
            <h5>Gulshan-1, Dhaka</h5>
            <h5>02-48155, +8801673110720</h5>
            <h5>www.smartinvoice.com</h5>

        </div>
        <div class="col-md-4 pull-right" style="margin-top: 85px">

            <?php
            echo '<h2>Invoice No. ' . $invoiceId . '</h2>';
            echo '<h4>Date ' . $date;
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h3>Bill To</h3>
            <?php
            echo '<h4>' . $customerName . '</h4>';
            echo '<h4>' . $customerAddress . '</h4>';
            ?>
        </div>
        <div class="col-md-6 pull-right">
            <h3>Ship to</h3>
            <?php
            echo '<h4>' . $shippingName . '</h4>';
            echo '<h4>' . $shippingAddress . '</h4>';
            ?>
        </div>
    </div>

    <hr>
    <div class="row">
        <div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Warranty</th>
                    <th>Manu. ID</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    echo '<tr>';
                    foreach($items as $singleItem){
                        echo '<td>' .$singleItem['name'] . '</td>';
                        echo '<td>' .$singleItem['description'] . '</td>';
                        echo '<td>' .$singleItem['serial'] . '</td>';
                        echo '<td>' .$singleItem['warranty'] . '</td>';
                        echo '<td>' .$singleItem['manuid'] . '</td>';
                        echo '<td>' .$singleItem['price'] . '</td>';
                    }
                    echo '</tr>'
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 pull-right text-right">


            <div class="col-md-8 text-right">
                <h4>Subtotal</h4>
                <h4>Delivery Cost</h4>
                <h4>Total</h4>
            </div>

            <div class="col-md-4 pull-right text-right">
                <?php
                echo '<h4>'.(intval($total) - intval($deliveryCharge)) . '</h4>';
                if(isset($deliveryCharge)){
                    echo '<h4>'.$deliveryCharge . '</h4>';
                }else{
                    echo '<h4>0</h4>';
                }
                echo '<h4>'.$total. '</h4>';
                ?>
            </div>
        </div>
    </div>
</div>

<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>