<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/23/15
 * Time: 10:39 AM
 */

require_once('functions.php');

$name = null;
$desc = null;
$retail = null;
$wholesale = null;
$count = null;
if (isset($_POST['productname']) && isset($_POST['count'])) {

    $count = $_POST['count'];
    try {
        $connection = connect_db();
        $statement = $connection->prepare('
            SELECT
              Name,
              Description,
              WholesalePrice,
              RetailPrice
            FROM
              PRODUCT
            WHERE Name = :name');

        $statement->execute(array('name'=>$_POST['productname']));

        if($row = $statement->fetch(PDO::FETCH_ASSOC)){
            $name = $row['Name'];
            $desc = $row['Description'];
            $retail = $row['RetailPrice'];
            $wholesale = $row['WholesalePrice'];
        }


    } catch (PDOException $e) {
        echo $e;
    }
}
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" media="all" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">

    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery-barcode.js"></script>

</head>
<body>

<div class="container-fluid" style="margin: 50px; padding: 0px">


    <?php

    for($i=0; $i<$count; $i++){
        echo '<div class="col-md-3" style="border: dashed; margin: 3px">';
            echo '<div class="barcode col-md-4"></div>';
            echo '<div class="col-md-7">';
                echo '<h4>'.$name.'</h4>';
                echo '<h5>Retail '.$retail.'TK</h5>';
                echo '<h5>Wholesale '.$wholesale.'TK</h5>';
            echo '</div>';
        echo '</div>';
    }
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var counter =0;
        $('.barcode').each(function(){
            console.log('barcode');
            $(this).barcode('' + getRandomInt(), 'datamatrix', {
                moduleSize :7
            });
        });

        function getRandomInt() {
            return Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
        }
    })




</script>

</body>