<!DOCTYPE html>

<?php
session_start();
require_once('functions.php');

if (isset($_SESSION['login_email']) && isset($_SESSION['loggedOnUserId'])) {

    $saleCountToday = 0;
    $saleTodayAmountTotal = 0;
    $dateTodayFrom = date('Y-m-d ') . '00:00:00';
    $dateTodayTo = date('Y-m-d H:i:s');
    try{
        $connection = connect_db();
        $statement = $connection->prepare('
            SELECT
              *
            FROM
              `SELL_INVOICE`
            WHERE
              Date BETWEEN :from AND  :to ORDER BY Date
        ');

        $statement->execute(array('from'=>$dateTodayFrom, 'to'=>$dateTodayTo));

        while($row = $statement->fetch(PDO::FETCH_ASSOC)){
            $saleCountToday = $saleCountToday + $row['Count'];
            $saleTodayAmountTotal = $saleTodayAmountTotal + $row['Total'];

        }
    }catch (PDOException $e){

    }

} else { //user is not logged, shouldn't see this page
    // header("Location: index.html");

}
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs">
                                    <strong class="font-bold">
                                        <?php
                                        echo $_SESSION['loggedOnUserFullName'];
                                        ?>
                                    </strong>
                             </span>
                                <span class="text-muted text-xs block">
                                    <?php echo $_SESSION['designation'];?> <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="./endpoints/logout.php">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li class="active">
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>

                            <li>
                                <a href="wholesale.php">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="managecustomer.php">Add/Edit Customer</a>
                            </li>
                            <li>
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageproduct.php">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.php">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="serialtag.php">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>


    <div id="page-wrapper" class="gray-bg">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png"
                             width="200" height="37">
                    </a>
                </div>
            </div>
        </nav>
        <!--        <form>-->
        <div id="content">

            <h1>Today's Insight</h1>

            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <?php
                                echo '<div class="huge">'. $saleCountToday . '</div>'
                                ?>
                                <div>New Sales!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>



            <div class="col-lg-4 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <?php
                                echo '<div class="huge">'. $saleTodayAmountTotal . 'TK</div>'
                            ?>
                                <div>Total sale!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    Today's sale graph
                </div>
                <div class="ibox-content">
                    <canvas id="myChart" width="1000" height="350"></canvas>

                </div>

            </div>

        </div>

        <!--        </form>-->
    </div>

</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../bower_components/typeahead.js/dist/typeahead.bundle.min.js"
<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../bower_components/Chart.js/Chart.min.js"></script>
<script src="../assets/js/dashboard.js"></script>

</body>
</html>                                   