<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/15/15
 * Time: 10:04 PM
 */



function connect_db(){
    $pdo_connection_string = 'mysql:host=localhost;dbname=SmartInvoice';
    $db_username = 'siuser';
    $db_pass = 'shouldBeChanged';
    $connection = new PDO($pdo_connection_string, $db_username, $db_pass);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $connection;
}