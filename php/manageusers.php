<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/16/15
 * Time: 1:33 AM
 */

?>

<!DOCTYPE html>

<?php
session_start();
if (isset($_SESSION['login_email'])) { //user is logged in


} else { //user is not logged, shouldn't see this page
    // header("Location: index.html");

}
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>
<div id="wrapper">


    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                        Williams</strong>
                             </span> <span class="text-muted text-xs block">Sales Team <b
                                        class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="./endpoints/logout.php">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li>
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>
                            <li>
                                <a href="wholesale.php">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li class="active">
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="managecustomer.php">Add/Edit Customer</a>
                            </li>
                            <li >
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageproduct.php">Add/Edit Product Items</a>
                            </li>
                            <li class="active">
                                <a href="#">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>

                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="serialtag.php">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!--end navbar and sidebar contents-->

    <div id="page-wrapper" class="gray-bg">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png"
                             width="200" height="37">
                    </a>
                </div>
            </div>
        </nav>

        <h1>Manage Users</h1>

        <!--available suppliers -->
        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Available Users
                    </div>
                    <div class="ibox-content">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" style="padding-bottom: 0px">

                            <table id="data-table" class="table table-striped table-bordered " cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Email</th>
                                    <th>FullName</th>
                                    <th>Designation</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <button id="removeUsers" class="btn btn-danger">Remove Selected User</button>
                    </div>
                </div>
            </div>

            <!--end available customers-->

            <!--add new customers-->
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        Add new User
                    </div>
                    <div class="ibox-content">
                        <div class="">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input id='nameInput' class="form-control" type="text" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input id='addressInput' type="email" class="form-control"
                                          placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="passInput" class="form-control" type="text"
                                       placeholder="Password">
                            </div>

                            <div class="form-group">
                                <label>Designation</label>
                                <input id="designationInput" class="form-control" type="text"
                                       placeholder="Designation">
                            </div>

                            <div class="form-group">
                                <button id="saveNewUserButton" class="btn btn-danger col-md-3">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--end add new user-->
        </div>
    </div>
</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../assets/js/manageuser.js"></script>

</body>
</html>