<html lang="en">


<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/salereport.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

</head>
<body>
<div id="wrapper">

    <!--navbar and sidebar contents-->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">SmartInvoice</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>

                    <li class="divider"></li>
                    <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retail.html">Retail</a>
                            </li>
                            <li>
                                <a href="wholesale.html">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href=managecustomer.html">Add/Edit Customer</a>
                            </li>
                            <li>
                                <a href="managesupplier.html">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageitems.html">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.html">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!--end navbar and sidebar contents-->

    <form>
        <div id="page-wrapper">
            <!--criteria panel-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Report criteria
                </div>

                <div class="panel-body">
                    <div class="col-md-6"> <!--left side panel-->
                        <div class="row row-padded"> <!--customer row-->
                            <div class="col-md-2 ">
                                <label class="top-margin" for="customerName">Customer</label>
                            </div>
                            <div class="col-md-10">
                                <input class="form-control " id="customerName" placeholder="Customer">
                            </div>
                        </div>
                        <!--customer row ends-->

                        <div class="row row-padded"> <!--serial row-->
                            <div class="col-md-2  ">
                                <label class="top-margin">Serial</label>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control " placeholder="From">
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " placeholder="To">
                            </div>
                        </div>
                        <!--serial row ends-->

                        <div class="row row-padded"> <!--price row starts-->
                            <div class="col-md-2">
                                <label class="top-margin">Price</label>
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="priceFrom" placeholder="From">
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="priceTo" placeholder="To">
                            </div>
                        </div>
                        <!--price row ends-->

                    </div>

                    <div class="col-md-6"> <!--right side panel-->

                        <div class="row row-padded"> <!--product row-->
                            <div class="col-md-2 ">
                                <label class="top-margin" for="productName">Product</label>
                            </div>
                            <div class="col-md-10">
                                <input class="form-control " id="productName" placeholder="Product name">
                            </div>
                        </div>
                        <!--product row ends-->

                        <div class="row row-padded"> <!--invoice row starts-->
                            <div class="col-md-2">
                                <label class="top-margin">Invoice No.</label>
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="invoiceFrom" placeholder="From">
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="invoiceTo" placeholder="To">
                            </div>
                        </div>
                        <!--invoice row ends-->

                        <div class="row row-padded"> <!--date row starts-->
                            <div class="col-md-2">
                                <label class="top-margin">Date</label>
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="dateFrom" placeholder="From">
                            </div>

                            <div class="col-md-5">
                                <input class="form-control " name="dateTo" placeholder="To">
                            </div>
                        </div>
                        <!--date row ends-->

                    </div>
                </div>
            </div>
            <!--end criteria panel-->

            <!--checkbox panel-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Report output parameters
                </div>

                <div class="panel-body">
                    <div class="row "> <!--checkbox row starts-->
                        <div class="col-md-12" >
                            <label class="checkbox-inline"><strong>Select output columns</strong</label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Serial
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Product
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Customer
                            </label>

                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Price
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Date
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Warranty
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Type
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Invoice ID
                            </label>
                        </div>
                    </div> <!--checkbox row ends-->

                </div>
            </div>
            <!--end checkbox panel-->

            <div class="row top-margin">
                <button class="btn btn-primary col-md-2 col-md-offset-5">Generate Report</button>
            </div>
        </div>
    </form>
</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../assets/js/dataTables.bootstrap.js"></script>
<script src="../assets/js/salereport.js"></script>

</body>
</html>
