<?php
/**
 * Created by PhpStorm.
 * User: muntasir
 * Date: 3/23/15
 * Time: 4:44 AM
 */


session_start();
if (isset($_SESSION['login_email'])) { //user is logged in


} else { //user is not logged, shouldn't see this page
    // header("Location: index.html");

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard . SmartInvoice</title>

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/metisMenu/dist/metisMenu.min.css">
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../assets/css/sb-admin-2.css">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="65px" height="60px" class="img-circle"
                                 src="http://d1oi7t5trwfj5d.cloudfront.net/32/c4/2217cd7d4775b663e3c2fb4d2ce8/emma-stone.jpg"/>
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David
                                        Williams</strong>
                             </span> <span class="text-muted text-xs block">Sales Team <b
                                        class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="./endpoints/logout.php">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>

                    <li class="active">
                        <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-usd fa-fw"></i> Sales<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="retailsale.php">Retail</a>
                            </li>

                            <li>
                                <a href="wholesale.php">Wholesale</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="addstock.php"><i class="fa fa-table fa-fw"></i> Stock Entry</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Manage<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="managecustomer.php">Add/Edit Customer</a>
                            </li>
                            <li>
                                <a href="managesupplier.php">Add/Edit Supplier</a>
                            </li>
                            <li>
                                <a href="manageproduct.php">Add/Edit Product Items</a>
                            </li>
                            <li>
                                <a href="manageusers.php">Add/Edit Users</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Product Item wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                            <li>
                                <a href="login.html">Invoice wise Sales Report</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-puzzle-piece fa-fw"></i> Tools<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="blank.html">Serial Tag Generator</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>


    <div id="page-wrapper" class="gray-bg">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="https://www.waveapps.com/sitestatic/public/img/wave-media-logo.png"
                             width="200" height="37">
                    </a>
                </div>


            </div>
        </nav>
        <!--        <form>-->
        <div id="content">
            <!-- Button trigger modal -->
            <form method="post" action="serialtaggenerator.php">
                <div class="form-group">
                    <select name="productname" class="form-control">
                        <?php
                        require_once('functions.php');

                        $connection = connect_db();
                        $statement = $connection->prepare('SELECT Name FROM PRODUCT');
                        $statement->execute();

                        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                            echo '<option>' . $row['Name'] . '</option>';
                        }
                        ?>

                    </select>

                </div>
                <div class="form-group">
                    <input name="count" class="form-control" placeholder="No. of tags to generate">
                </div>

                <button type="submit" class="btn btn-primary" value="Print">Print</button>

            </form>


        </div>

        <!--        </form>-->
    </div>

</div>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/sb-admin-2.js"></script>
<script src="../bower_components/typeahead.js/dist/typeahead.bundle.min.js"
<script src="../assets/js/dataTables.bootstrap.js"></script>

</body>
</html>