/**
 * Created by muntasir on 3/22/15.
 */
$(document).ready(function () {

    var totalField = $('#total');
    var subtotalField = $('#subtotal');
    var deliveryCostfield = $('#delivery');
    var customerField = $('#customerName');
    var shippingName = $('#shippingname');
    var shippingAddress = $('#shippingaddress');

    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false

    });

    var $serialInput = $('#serialInput');
    $('#addRow').on('click', function () {
        console.log('test');

        $.ajax({
            url: '../php/endpoints/searchstock.php',
            type: 'GET',
            data: {serial: $serialInput.val()},
            success: function (items) {
                /* this hack is applied to convert php json object response
                 * to an array. because addItemToTable() expects an array of objects*/

                var itemsArray = [];
                itemsArray.push(items);

                addItemToTable(itemsArray, table);
            },
            error: function (err) {
                alert("Invalid serial number. This item is already sold or never existed");
            }
        });
    });

    $('#saveBtn').click(function () {
        var itemSerials = [];

        //fetch serials from datatable
        var rows = $("#data-table").dataTable().fnGetNodes();
        for (var i = 0; i < rows.length; i++) {
            var serialNo = $(rows[i]).find("td:eq(2)").html();
            itemSerials.push(serialNo);
        }

        $.ajax({
            url: '../php/endpoints/sale.php',
            type: 'GET',
            data: {
                'new': {
                    'customer': customerField.val(),
                    'shippingname': shippingName.val(),
                    'shippingaddress': shippingAddress.val(),
                    'items': itemSerials,
                    'total': totalField.val(),
                    'delivery': deliveryCostfield.val(),
                    'type': 'wholesale'
                }
            },
            success: function () {
                $('#saveBtn').attr("disabled", true);
                alert('Sale successfully recorded');
            },
            error: function (err) {
                alert("Invalid serial number. This item is already sold or never existed");
            }
        });
    });

    subtotalField.change(function () {
        updateTotalField();
    });

    deliveryCostfield.change(function () {
        updateTotalField();
    });




    function addItemToTable(items, table) {
        $.each(items, function (i, item) {
            console.log('in add item to table ');

            table.row.add([
                item.name,
                item.description,
                item.serial,
                item.manid,
                item.warranty,
                item.price
            ]).draw();

            var subtotalStr = subtotalField.val();
            var subtotalInt = parseInt(subtotalStr);
            subtotalInt = subtotalInt + parseInt(item.price);
            subtotalField.val(subtotalInt);

            updateTotalField();
        });
    }

    function updateTotalField() {
        var subtotalInt = parseInt(subtotalField.val());
        var deliveryInt = parseInt(deliveryCostfield.val());

        totalField.val(subtotalInt + deliveryInt);
    }

});
