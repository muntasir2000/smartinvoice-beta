/**
 * Created by muntasir on 3/19/15.
 */
/**
 * Created by muntasir on 3/14/15.
 */

$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false

    });

    $('#data-table tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#removeCustomer').click(function () {

        console.log(table.row('.selected').data()[4]);
        $.ajax({
            url: '../php/endpoints/customer.php',
            type: 'POST',
            data: {customerId: table.row('.selected').data()[0]},
            success: function (response) {
                console.log(response);
                alert("Products deleted successfully");
                table.row('.selected').remove().draw(false);

            },
            error: function (err) {
                alert('Error deleting products. No change was made');
            }
        });

    });

    $('#saveNewCustomerButton').click(function () {

        $.ajax({
            url: '../php/endpoints/customer.php',
            type: 'GET',
            data: {
                'newcustomer': {
                    'name': $('#nameInput').val(),
                    'address': $('#addressInput').val(),
                    'contact': $('#passInput').val()
                }
            },
            success: function () {
                table.row.add([
                        'RELOAD TO VIEW',
                        $('#nameInput').val(),
                        $('#addressInput').val(),
                        $('#passInput').val()]
                ).draw();
                alert($('#nameInput').val() + 'Successfully added');
            },
            error: function (err) {

                alert('Error saving new Product' + $('#nameInput').val() +
                '    ' + err);
            }
        });
    });


    loadExistingCustomer();


    function addItemToTable(items, table) {
        $.each(items, function (i, item) {
            table.row.add([
                item.id,
                item.name,
                item.address,
                item.contact
            ]).draw();

            console.log('test');
        });
    }


    function loadExistingCustomer() {
        console.log('test');

        $.ajax({
            url: '../php/endpoints/customer.php',
            type: 'GET',
            data: {loadCustomer: true},
            success: function (items) {
                console.log(items);
                addItemToTable(items, table);

            },
            error: function (err) {
                alert('error loading existing product to table' + err);
            }
        });

    };


});