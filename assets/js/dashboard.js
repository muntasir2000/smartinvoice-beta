/**
 * Created by muntasir on 3/4/15.
 */

$(document).ready(function(){
    var ctx = $("#myChart").get(0).getContext("2d");

    var data = {
        labels: ["12AM", "3AM", "6AM", "10AM", "2PM", "6PM", "12PM"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }
        ]
    };

    var data2 = {
        labels : ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
        datasets : [
            {
                fillColor : "rgba(99,123,133,0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                data : [65,54,30,81,56,55,40]
            },
            {
                fillColor : "rgba(219,186,52,0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                data : [20,60,42,58,31,21,50]
            },
        ]
    }
    var myLineChart = new Chart(ctx).Line(data, {});

});