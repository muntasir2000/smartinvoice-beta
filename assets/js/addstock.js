/**
 * Created by muntasir on 3/20/15.
 */




var productMap = {};

$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false

    });


    loadProducts();
    addColorStateListeners();
    $('#productName').change(function () {
        var selected = $("#productName option:selected").text();
        console.log(selected);
        $('#descriptionInput').val(productMap[selected]);
    });

    $('#addNewItemBtn').click(function () {
        if (varifyInputAddNewItem() !== false) {
            return;
        }
        console.log(varifyInputAddNewItem());

        table.row.add([nameInput.val(), descInput.val(), serialInput.val(), manIdInput.val(), warrantyInput.val() === '' ? 0 : warrantyInput.val(), priceInput.val()]).draw();
        var total = parseInt(totalPrice.val());
        totalPrice.val(parseInt(priceInput.val()) + total);
    });

    /*
     save to database
     */
    $('#saveBtn').click(function () {
        var supplierName = $('#supplierName').val();
        var date = $('#dateInput').val();
        console.log(supplierName + ' ' + date);
        var total = totalPrice.val();

        var tableData = [];
        var rows = $("#data-table").dataTable().fnGetNodes();
        for (var i = 0; i < rows.length; i++) {
            var productName = $(rows[i]).find("td:eq(0)").html();
            var serialNo = $(rows[i]).find("td:eq(2)").html();
            var manuId = $(rows[i]).find("td:eq(3)").html();
            var warranty = $(rows[i]).find("td:eq(4)").html();
            var cost = $(rows[i]).find("td:eq(5)").html();

            tableData.push({
                'name': productName,
                'serial': serialNo,
                'manuId': manuId,
                'warranty': warranty,
                'cost': cost
            });
        }
        console.log(tableData);


        $.ajax({
            url: '../php/endpoints/stock.php',
            type: 'GET',
            data: {
                'new': {
                    'supplier': supplierName,
                    'date': date,
                    'items': tableData,
                    'total': total
                }
            },
            success: function () {
                alert('successfully added to db');
                $('#saveBtn').attr("disabled", true);
            },
            error: function (err) {
                alert('error saving to db')
            }
        });


    });

});
var nameInput = $('#productName');
var totalPrice = $('#totalPrice');

var descInput = $('#descriptionInput');
var serialInput = $('#serialInput');
var manIdInput = $('#manIdInput');
var priceInput = $('#priceInput');
var warrantyInput = $('#inputWarranty');

function varifyInputAddNewItem() {
    var error = false;
    if (descInput.val() === '') {
        descInput.css('border-color', '#ec4758');
        error = true;
    }
    if (serialInput.val() === '') {
        serialInput.css('border-color', '#ec4758');
        error = true;
    }
    if (manIdInput.val() === '') {
        manIdInput.css('border-color', '#ec4758');
        error = true;
    }
    if (priceInput.val() === '') {
        priceInput.css('border-color', '#ec4758');
        error = true;
    }
    return error;
}

function addColorStateListeners() {
    descInput.change(function () {
        descInput.css('border-color', '#c1c2c3');
    });
    serialInput.change(function () {
        serialInput.css('bordre-color', '#c1c2c3');
    });
    manIdInput.change(function () {
        manIdInput.css('bordre-color', '#c1c2c3');
    });
    priceInput.change(function () {
        priceInput.css('bordre-color', '#c1c2c3');
    });
}

function insertProduct(key, value) {
    console.log(key + value);
    productMap[key] = value;
}

function loadProducts() {
    $.ajax({
        url: '../php/endpoints/product.php',
        type: 'GET',
        data: {loadproducts: true},
        success: function (items) {
            console.log(items);

            for (var i = 0; i < items.length; i++) {
                insertProduct(items[i].name, items[i].desc);
            }
        },
        error: function (err) {
            alert('error loading existing product to table' + err);
        }
    });
    console.log(productMap);
}

function addItemToTable(items, table) {
    $.each(items, function (i, item) {
        console.log('in add item to table ');

        table.row.add([
            item.name,
            item.desc,
            item.serial,
            item.man,
            item.warranty,
            item.price
        ]).draw();
    });
}



