/**
 * Created by muntasir on 3/23/15.
 */
/**
 * Created by muntasir on 3/19/15.
 */
/**
 * Created by muntasir on 3/14/15.
 */

$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false
    });

    $('#data-table tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#removeUsers').click(function () {

        console.log(table.row('.selected').data()[4]);
        $.ajax({
            url: '../php/endpoints/user.php',
            type: 'POST',
            data: {userId: table.row('.selected').data()[0]},
            success: function (response) {
                console.log(response);
                alert("Products deleted successfully");
                table.row('.selected').remove().draw(false);

            },
            error: function (err) {
                alert('Error deleting products. No change was made');
            }
        });

    });

    $('#saveNewUserButton').click(function () {

        $.ajax({
            url: '../php/endpoints/user.php',
            type: 'GET',
            data: {
                'newuser': {
                    'fullname': $('#nameInput').val(),
                    'email': $('#addressInput').val(),
                    'pass': $('#passInput').val(),
                    'designation': $('#designationInput').val()
                }
            },
            success: function () {
                table.row.add([
                        'RELOAD TO VIEW',
                        $('#nameInput').val(),
                        $('#addressInput').val(),
                        $('#designationInput').val()]
                ).draw();
                alert($('#nameInput').val() + 'Successfully added');
            },
            error: function (err) {

                alert('Error saving new Product' + $('#nameInput').val() +
                '    ' + err);
            }
        });
    });


    loadExistingUsers();


    function addItemToTable(items, table) {
        $.each(items, function (i, item) {
            table.row.add([
                item.id,
                item.email,
                item.fullname,
                item.designation
            ]).draw();

            console.log('test');
        });
    }


    function loadExistingUsers() {
        console.log('test');

        $.ajax({
            url: '../php/endpoints/user.php',
            type: 'GET',
            data: {loadUser: true},
            success: function (items) {
                console.log(items);
                addItemToTable(items, table);
            },
            error: function (err) {
                console.log(err);
                alert('error loading existing product to table' + err);
            }
        });
    };


});