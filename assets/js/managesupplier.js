/**
 * Created by muntasir on 3/16/15.
 */

$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false
    });
    table.column(0).visible(false);

    $('#data-table tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#removeSupplier').click(function () {

        console.log(table.row('.selected').data()[4]);
        $.ajax({
            url: '../php/endpoints/supplier.php',
            type: 'POST',
            data: {'supplierId': table.row('.selected').data()[0]},
            success: function (response) {
                console.log(response);
                alert("Supplier deleted successfully");
                table.row('.selected').remove().draw(false);

            },
            error: function (err) {
                alert('Error deleting products. No change was made');
            }
        });

    });

    $('#saveNewSupplierButton').click(function () {

        $.ajax({
            url: '../php/endpoints/supplier.php',
            type: 'GET',
            data: {
                'newsupplier': {
                    'name': $('#nameInput').val(),
                    'address': $('#addressInput').val(),
                    'contact': $('#passInput').val()
                }
            },
            success: function () {
               /* table.row.add([
                        $('#nameInput').val(),
                        $('#addressInput').val(),
                        $('#contactInput').val()
                    ]
                ).draw();*/
                loadExistingSuppliers();
                alert($('#nameInput').val() + 'Successfully added');
            },
            error: function (err) {

                alert('Error saving new Supplier' + $('#nameInput').val() +
                '    ' + err);
            }
        });
    });


    loadExistingSuppliers();


    function addItemToTable(items, table) {
        $.each(items, function (i, item) {
            table.row.add([
                item.id,
                item.name,
                item.address,
                item.contact,

            ]).draw();
            console.log('test');
        });
    }


    function loadExistingSuppliers() {
        console.log('test');

        $.ajax({
            url: '../php/endpoints/supplier.php',
            type: 'GET',
            data: {loadSupplier: true},
            success: function (items) {
                addItemToTable(items, table);
            },
            error: function (err) {
                alert('error loading existing product to table' + err);
            }
        });

    };


});