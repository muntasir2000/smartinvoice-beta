/**
 * Created by muntasir on 3/14/15.
 */

$(document).ready(function () {
    var table = $('#data-table').DataTable({
        "paging": false,
        "info": false,
        "filter": false

    });

    $('#data-table tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#removeProduct').click(function () {

        console.log(table.row('.selected').data()[4]);
        $.ajax({
            url: '../php/endpoints/product.php',
            type: 'POST',
            data: {product_code: table.row('.selected').data()[4]},
            success: function (response) {
                console.log(response);
                alert("Products deleted successfully");
                table.row('.selected').remove().draw(false);

            },
            error: function (err) {
                alert('Error deleting products. No change was made');
            }
        });

    });

    $('#saveNewProductButton').click(function () {

        $.ajax({
            url: '../php/endpoints/product.php',
            type: 'GET',
            data: {
                'newproduct': {
                    'name': $('#nameInput').val(),
                    'description': $('#descInput').val(),
                    'wholesale': $('#wholesaleInput').val(),
                    'retail': $('#retailInput').val(),
                    'productcode': $('#productCodeInput').val()
                }
            },
            success: function () {
                table.row.add([
                    $('#nameInput').val(),
                    $('#descInput').val(),
                    $('#wholesaleInput').val(),
                    $('#retailInput').val(),
                    $('#productCodeInput').val()]
                ).draw();
                alert($('#nameInput').val() + 'Successfully added');
            },
            error: function (err) {

                alert('Error saving new Product' + $('#nameInput').val() +
                '    ' + err);
            }
        });
    });


    loadExistingProducts();


    function addItemToTable(items, table) {
        $.each(items, function (i, item) {
            table.row.add([
                item.name,
                item.desc,
                item.wholesale,
                item.retail,
                item.productcode
            ]).draw();
            table.row.add
            console.log('test');
        });
    }


    function loadExistingProducts() {
        console.log('test');

        $.ajax({
            url: '../php/endpoints/product.php',
            type: 'GET',
            data: {loadproducts: true},
            success: function (items) {
                console.log(items);
                addItemToTable(items, table);

            },
            error: function (err) {
                alert('error loading existing product to table' + err);
            }
        });

    };

    /* function getSelectedRowsProductIdsAsArray(){
     var selectedRowProductCodes = new Array();
     console.log('in get function iterator');

     //    console.log(table.row('.selected').data());
     table.row('.selected').iterator('row', function(context, index){
     var r = table.row(index).data();
     console.log(r);
     selectedRowProductCodes.push(r[4]);
     });
     return selectedRowProductCodes;
     }*/

});